import { HTMLAttributes } from "react";
export interface ICard extends HTMLAttributes<HTMLDivElement> {
}
declare const Card: ((props: ICard) => JSX.Element) & {
    displayName: string;
};
export default Card;
