import { HTMLAttributes } from "react";
export interface IRowLayout extends HTMLAttributes<HTMLDivElement> {
}
export declare const RowLayout: (props: IRowLayout) => JSX.Element;
