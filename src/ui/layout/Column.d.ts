import { HTMLAttributes } from "react";
export interface IColumnLayout extends HTMLAttributes<HTMLDivElement> {
}
export declare const ColumnLayout: (props: IColumnLayout) => JSX.Element;
