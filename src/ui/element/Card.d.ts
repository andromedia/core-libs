import { HTMLAttributes } from "react";
export interface ICardElement extends HTMLAttributes<HTMLDivElement> {
}
export declare const CardElement: ((props: ICardElement) => JSX.Element) & {
    displayName: string;
};
