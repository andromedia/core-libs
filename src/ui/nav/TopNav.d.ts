import { HTMLAttributes } from "react";
export interface ITopNav extends HTMLAttributes<HTMLDivElement> {
}
export declare const TopNav: (props: ITopNav) => JSX.Element;
