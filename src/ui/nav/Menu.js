"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Menu = void 0;
var react_1 = require("@fluentui/react");
var Icons = __importStar(require("heroicons-react"));
var mobx_react_lite_1 = require("mobx-react-lite");
var react_2 = __importDefault(require("react"));
exports.Menu = mobx_react_lite_1.observer(function (props) {
    return (react_2.default.createElement(react_1.Nav, __assign({ selectedKey: "key3", styles: {
            link: {
                outline: "0px !important",
            },
        }, onRenderLink: function (props) {
            var _a;
            var Icon = !!(props === null || props === void 0 ? void 0 : props.isExpanded)
                ? Icons.ChevronUpOutline
                : Icons.ChevronDownOutline;
            return (react_2.default.createElement("div", { className: "flex flex-1 flex-row items-center" },
                react_2.default.createElement("div", { className: "flex flex-1" }, props === null || props === void 0 ? void 0 : props.name),
                !!((_a = props === null || props === void 0 ? void 0 : props.links) === null || _a === void 0 ? void 0 : _a.length) && (react_2.default.createElement(Icon, { className: "text-gray-600", size: 16 }))));
        } }, props)));
});
