/// <reference types="react" />
import { INavProps } from "@fluentui/react";
export interface IMenu extends INavProps {
}
export declare const Menu: ((props: IMenu) => JSX.Element) & {
    displayName: string;
};
