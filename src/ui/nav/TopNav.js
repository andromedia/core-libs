"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TopNav = void 0;
var react_1 = __importDefault(require("react"));
exports.TopNav = function (props) {
    return (react_1.default.createElement("div", __assign({ className: "flex flex-row items-center bg-white h-12 border-b border-gray-300 px-4 z-30" }, props)));
};
