import { Model } from "./model";
export interface HasManyObject<T extends Model> {
    list: T[];
    load: (where?: IWhere<T>) => Promise<void>;
}
interface ILoadOptions<T extends Model> {
    fetch: (where?: IWhere<T>) => Promise<T[]>;
    create: (props: any) => T;
    where?: IWhere<T>;
}
interface IWhere<T> {
}
export interface HasManyOptions<T extends Model<Model<K>>, K extends Model> {
    init?: (hasmany: HasManyClass<T, K>) => void;
    autoload?: boolean;
    constructor?: Function;
    list?: T[];
    load?: (opt?: ILoadOptions<T>) => Promise<T[]>;
}
export declare class HasManyClass<T extends Model<Model<K>>, K extends Model> implements HasManyObject<T> {
    static __type: string;
    opt: HasManyOptions<T, K>;
    list: T[];
    parent: K;
    private creator;
    constructor(parent: K, creator: new () => T, opt?: HasManyOptions<T, K>);
    protected init(opt?: HasManyOptions<T, K>): void;
    create: (props?: any, mapping?: any) => T;
    load(where?: IWhere<T>): Promise<void>;
}
export {};
