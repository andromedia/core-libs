"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Model = void 0;
var mobx_1 = require("mobx");
var router_1 = __importDefault(require("next/router"));
var hasmany_1 = require("./hasmany");
var Model = /** @class */ (function () {
    function Model(options) {
        this._opt = {};
        // private static _primaryKey = "id";
        // private static _tableName = "";
        // private static _modelName = "";
        this._init = false;
        this._observedProperties = [];
        this._reactions = [];
    }
    Model.create = function (options) {
        var obj = new this(options !== null && options !== void 0 ? options : {});
        if (options) {
            obj._opt = options;
        }
        obj._initMobx(obj);
        return obj;
    };
    Model.childOf = function (parent, options) {
        var obj = new this(options !== null && options !== void 0 ? options : {});
        if (options) {
            obj._opt = options;
        }
        obj._parent = parent;
        return obj;
    };
    Model.prototype._hasMany = function (modelCTor, opt, ext) {
        var ctor = modelCTor;
        var autoload = false;
        var parentOpt = this._opt;
        if (parentOpt.autoload !== undefined) {
            if (!opt || (opt && opt.autoload === undefined)) {
                autoload = parentOpt.autoload;
            }
        }
        var current = new hasmany_1.HasManyClass(this, ctor, __assign(__assign({}, opt), { autoload: autoload }));
        for (var i in ext) {
            if (current[i] === undefined) {
                if (typeof ext[i] === "function") {
                    current[i] = ext[i].bind(current);
                }
                else {
                    current[i] = ext[i];
                }
            }
        }
        return current;
    };
    Model.prototype._initMobx = function (self) {
        var _this = this;
        if (self._init)
            return;
        var obj = {};
        var props = [];
        for (var _i = 0, _a = Object.getOwnPropertyNames(self); _i < _a.length; _i++) {
            var i = _a[_i];
            if (i.indexOf("_") === 0)
                continue;
            var val = self[i];
            var isObservable = true;
            var type = getType(val);
            switch (type) {
                case "model":
                    props.push(i);
                    if (val._parent === self) {
                        if (!!this._opt.localStorage) {
                            val._opt.localStorage = true;
                        }
                        val._initMobx(val);
                        isObservable = false;
                    }
                    break;
            }
            if (isObservable) {
                if (typeof val !== "function") {
                    obj[i] = mobx_1.observable;
                    props.push(i);
                }
            }
            self._init = true;
        }
        var methods = Object.getOwnPropertyNames(Object.getPrototypeOf(self));
        for (var _b = 0, methods_1 = methods; _b < methods_1.length; _b++) {
            var i = methods_1[_b];
            if (i !== "constructor" && i.indexOf("_") !== 0) {
                if (typeof self[i] === "function") {
                    obj[i] = mobx_1.action;
                }
                else {
                    obj[i] = mobx_1.computed;
                }
            }
        }
        mobx_1.makeObservable(this, obj);
        if (!!this._opt.localStorage) {
            this._opt.localStorage = true;
            if (!this._parent) {
                // children model will be loaded from it's parent
                // so, we load only the parent
                // and it will extract it's value to it's children.
                this.loadFromLocalStorage(props);
            }
            props.forEach(function (e) {
                _this.observeProperty(e);
            });
        }
    };
    Model.prototype.observeProperty = function (key) {
        var _this = this;
        if (!!key && key[0] === "_")
            return;
        if (!this._observedProperties)
            this._observedProperties = [];
        var prop = this[key];
        if (prop instanceof Model) {
            prop._addReaction(function () {
                _this.saveToLocalStorage(_this._json);
            });
        }
        else {
            this._observedProperties.push(mobx_1.reaction(function () { return _this[key]; }, function () {
                if (!_this._parent) {
                    _this.saveToLocalStorage(_this._json);
                }
                else {
                    _this._reactions.forEach(function (e) {
                        e();
                    });
                }
            }));
        }
    };
    Model.prototype._addReaction = function (fun) {
        if (!this._reactions)
            this._reactions = [];
        this._reactions.push(fun);
    };
    Model.prototype._destroy = function () {
        this._observedProperties.forEach(function (e) {
            e();
        });
        for (var i in this) {
            if (this[i] instanceof Model) {
                this[i]._destroy();
            }
        }
    };
    Model.prototype.loadFromLocalStorage = function (props) {
        return __awaiter(this, void 0, void 0, function () {
            var storeName, json, content;
            return __generator(this, function (_a) {
                storeName = this._opt.storageName || this.constructor.name;
                if (typeof window !== "undefined") {
                    json = window.localStorage.getItem(storeName);
                }
                try {
                    content = JSON.parse(json || "{}");
                    this._loadJSON(content);
                }
                catch (e) { }
                return [2 /*return*/];
            });
        });
    };
    Model.prototype.saveToLocalStorage = function (obj) {
        var storeName = this._opt.storageName || this.constructor.name;
        if (typeof window !== "undefined") {
            window.localStorage.setItem(storeName, JSON.stringify(obj));
        }
        // console.log("Saving to local storage: ", storeName, obj);
    };
    Object.defineProperty(Model.prototype, "_json", {
        get: function () {
            var _this = this;
            var result = {};
            var self = this;
            var _loop_1 = function (i) {
                if (i !== "constructor" &&
                    i.indexOf("_") !== 0 &&
                    typeof self[i] !== "function") {
                    if (self[i] instanceof Model) {
                        if (self[i].constructor === this_1.constructor) {
                            return { value: JSON.stringify(self[i]) };
                        }
                        result[i] = self[i]._json;
                    }
                    else if (typeof self[i] === "object") {
                        if (this_1[i] instanceof hasmany_1.HasManyClass) {
                            result[i] = this_1[i].list.map(function (e, idx) {
                                if (!(e instanceof Model)) {
                                    throw new Error(i + ".list[" + idx + "] is not a Model (current type: " + typeof e + "). Please check your code where you change " + i + ".list");
                                }
                                else {
                                    if (e.constructor === _this.constructor) {
                                        return JSON.stringify(e);
                                    }
                                }
                                return e._json;
                            });
                        }
                        else {
                            result[i] = mobx_1.toJS(self[i]);
                        }
                    }
                    else {
                        result[i] = self[i];
                    }
                }
            };
            var this_1 = this;
            for (var _i = 0, _a = Object.getOwnPropertyNames(self); _i < _a.length; _i++) {
                var i = _a[_i];
                var state_1 = _loop_1(i);
                if (typeof state_1 === "object")
                    return state_1.value;
            }
            return result;
        },
        enumerable: false,
        configurable: true
    });
    Model.prototype._load = function () {
        return __awaiter(this, void 0, void 0, function () {
            var value;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        value = {};
                        if (!this._beforeLoad) return [3 /*break*/, 2];
                        return [4 /*yield*/, this._beforeLoad(value)];
                    case 1:
                        value = _a.sent();
                        _a.label = 2;
                    case 2:
                        this._loadJSON(value);
                        if (!this._afterLoad) return [3 /*break*/, 4];
                        return [4 /*yield*/, this._afterLoad(this)];
                    case 3:
                        _a.sent();
                        _a.label = 4;
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    Model.prototype._loadJSON = function (obj, mapping) {
        var _this = this;
        var value = obj;
        var applyValue = function (key, value, processValue) {
            if (typeof processValue === "function") {
                return processValue(value, _this[key]);
            }
            return value;
        };
        var i;
        var _loop_2 = function () {
            var key = i;
            var valueMeta = undefined;
            if (mapping) {
                if (mapping[i]) {
                    if (Array.isArray(mapping[i])) {
                        if (mapping[i].length === 2) {
                            key = mapping[i][0];
                            valueMeta = mapping[i][1];
                        }
                    }
                    else {
                        key = mapping[i];
                    }
                }
                else if (this_2[i] === undefined) {
                    return "continue";
                }
            }
            if (typeof this_2[key] !== "object") {
                mobx_1.runInAction(function () {
                    _this[key] = applyValue(key, value[i], valueMeta);
                });
            }
            else {
                if (this_2[key] instanceof Model) {
                    this_2[key]._loadJSON(applyValue(key, value[i], valueMeta));
                }
                else if (this_2[key] instanceof hasmany_1.HasManyClass) {
                    var c_1 = this_2[key];
                    var result = value[i].map(function (e) {
                        return c_1.create(e, valueMeta);
                    });
                    this_2[key].list = result;
                }
                else if (typeof value[i] !== "function") {
                    mobx_1.runInAction(function () {
                        _this[key] = applyValue(key, value[i], valueMeta);
                    });
                }
            }
        };
        var this_2 = this;
        for (i in value) {
            _loop_2();
        }
        return this;
    };
    Model.findMany = function (q, featureName) {
        if (featureName === void 0) { featureName = "public"; }
        return __awaiter(this, void 0, void 0, function () {
            var r, body, res;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        r = router_1.default.router;
                        if (!r) return [3 /*break*/, 3];
                        body = {
                            p: r.route,
                            f: featureName,
                            m: this._modelName,
                            t: this._tableName,
                            q: q,
                        };
                        return [4 /*yield*/, fetch(r.basePath + "/api/db/findMany", {
                                method: "POST",
                                body: JSON.stringify(body),
                            })];
                    case 1:
                        res = _a.sent();
                        return [4 /*yield*/, res.json()];
                    case 2: return [2 /*return*/, _a.sent()];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    Model.__type = "model";
    return Model;
}());
exports.Model = Model;
var getType = function (obj) {
    if (!!obj && typeof obj === "object") {
        var c = obj.constructor;
        if (c && c.__type) {
            return c.__type;
        }
    }
    return undefined;
};
