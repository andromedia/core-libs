import { HasManyClass, HasManyOptions } from "./hasmany";
export interface Type<T> extends Function {
    new (...args: any[]): T;
}
export interface ModelOptions {
    autoload?: boolean;
    localStorage?: boolean;
    storageName?: string;
}
export interface IQuery<T> {
    take?: number;
    skip?: number;
}
export declare abstract class Model<M extends Model = any> {
    _parent?: M;
    private _opt;
    private _init;
    constructor(options: ModelOptions);
    static create<T extends Model>(this: {
        new (options: ModelOptions): T;
    }, options?: ModelOptions): T;
    static childOf<T extends Model>(this: {
        new (options: ModelOptions): T;
    }, parent: Model, options?: ModelOptions): T;
    _hasMany<T extends Model<M>>(modelCTor: typeof Model, opt?: HasManyOptions<T, M>, ext?: any): HasManyClass<T, M>;
    private _initMobx;
    private _observedProperties;
    private observeProperty;
    private _reactions;
    protected _addReaction(fun: () => void): void;
    _destroy(): void;
    private loadFromLocalStorage;
    private saveToLocalStorage;
    get _json(): any;
    protected _beforeLoad?: (obj: any) => Promise<any>;
    protected _afterLoad?: (obj: Model) => Promise<void>;
    private _load;
    _loadJSON(obj: any, mapping?: any): this;
    static findMany(q: any, featureName?: string): Promise<any>;
    static __type: string;
}
