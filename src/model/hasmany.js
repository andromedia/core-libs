"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.HasManyClass = void 0;
var mobx_1 = require("mobx");
var HasManyClass = /** @class */ (function () {
    function HasManyClass(parent, creator, opt) {
        var _this = this;
        if (opt === void 0) { opt = {}; }
        this.list = [];
        this.create = function (props, mapping) {
            if (props === void 0) { props = {}; }
            var result = new _this.creator();
            result._parent = _this.parent;
            result._opt = __assign(__assign({}, result._opt), { autoload: _this.opt.autoload });
            if (result && result._loadJSON) {
                result._initMobx(result); //TODO: this should not privately access _initMobx >.<
                if (typeof mapping === "function") {
                    result._loadJSON(props, mapping(props));
                }
                else {
                    result._loadJSON(props, mapping);
                }
            }
            return result;
        };
        this.parent = parent;
        this.creator = creator;
        this.opt = opt;
    }
    HasManyClass.prototype.init = function (opt) {
        var _this = this;
        if (opt === void 0) { opt = {}; }
        Object.keys(opt).forEach(function (i) {
            _this.opt[i] = opt[i];
        });
        if (this.opt.list) {
            this.list = this.opt.list;
        }
        mobx_1.makeObservable(this, {
            list: mobx_1.observable,
        });
        if (!this.opt.init) {
            if (this.opt.autoload !== false) {
                this.load();
            }
        }
        else {
            this.opt.init(this);
        }
    };
    HasManyClass.prototype.load = function (where) {
        return __awaiter(this, void 0, void 0, function () {
            var fetch, result;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        fetch = function (where) { return __awaiter(_this, void 0, void 0, function () {
                            return __generator(this, function (_a) {
                                //TODO: Execute Actual data loading to server
                                return [2 /*return*/, []];
                            });
                        }); };
                        if (!this.opt.load) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.opt.load({
                                fetch: fetch,
                                create: this.create,
                                where: where,
                            })];
                    case 1:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 2: return [4 /*yield*/, fetch(where)];
                    case 3:
                        result = (_a.sent()).map(function (e) { return _this.create(e); });
                        _a.label = 4;
                    case 4:
                        mobx_1.runInAction(function () { return (_this.list = result); });
                        return [2 /*return*/];
                }
            });
        });
    };
    HasManyClass.__type = "hasmany";
    return HasManyClass;
}());
exports.HasManyClass = HasManyClass;
// export function HasMany<T extends Model<M>, M extends Model>(
//   parent: M,
//   target: T,
//   opt?: HasManyOptions<T, M>,
//   ext?: any
// ): HasManyClass<T, M> {
//   const creator: new () => T = <any>target.constructor;
//   let autoload = true;
//   const parentOpt = (parent as any)._opt;
//   if (parentOpt.autoload !== undefined) {
//     if (!opt || (opt && opt.autoload === undefined)) {
//       autoload = parentOpt.autoload;
//     }
//   }
//   const current: any = new HasManyClass<T, M>(parent, creator, {
//     ...opt,
//     autoload,
//   });
//   for (let i in ext) {
//     if (current[i] === undefined) {
//       if (typeof ext[i] === "function") {
//         current[i] = (ext[i] as any).bind(current);
//       } else {
//         current[i] = ext[i];
//       }
//     }
//   }
//   return current;
// }
