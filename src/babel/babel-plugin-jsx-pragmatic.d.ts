export default function jsxPragmatic(babel: any): {
    inherits: any;
    pre: () => void;
    visitor: {
        Program: {
            exit: (path: any, state: any) => void;
        };
        JSXElement: (path: any, state: any) => void;
        JSXFragment: (path: any, state: any) => void;
    };
};
