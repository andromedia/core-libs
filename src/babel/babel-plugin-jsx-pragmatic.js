"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var plugin_syntax_jsx_1 = __importDefault(require("@babel/plugin-syntax-jsx"));
var findLast = function (arr, predicate) {
    for (var i = arr.length - 1; i >= 0; i--) {
        if (predicate(arr[i])) {
            return arr[i];
        }
    }
};
function jsxPragmatic(babel) {
    var t = babel.types;
    function addPragmaImport(path, state) {
        var importDeclar = t.importDeclaration([
            t.importSpecifier(t.identifier(state.opts.import), t.identifier(state.opts.export || "default")),
        ], t.stringLiteral(state.opts.module));
        var targetPath = findLast(path.get("body"), function (p) {
            return p.isImportDeclaration();
        });
        if (targetPath) {
            targetPath.insertAfter([importDeclar]);
        }
        else {
            // Apparently it's now safe to do this even if Program begins with directives.
            path.unshiftContainer("body", importDeclar);
        }
    }
    return {
        inherits: plugin_syntax_jsx_1.default,
        pre: function () {
            if (!(this.opts.module && this.opts.import)) {
                throw new Error("@emotion/babel-plugin-jsx-pragmatic: You must specify `module` and `import`");
            }
        },
        visitor: {
            Program: {
                exit: function (path, state) {
                    if (!state.get("jsxDetected"))
                        return;
                    addPragmaImport(path, state);
                },
            },
            JSXElement: function (path, state) {
                state.set("jsxDetected", true);
            },
            JSXFragment: function (path, state) {
                state.set("jsxDetected", true);
            },
        },
    };
}
exports.default = jsxPragmatic;
