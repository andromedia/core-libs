"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var plugin_transform_react_jsx_1 = __importDefault(require("@babel/plugin-transform-react-jsx"));
var babel_plugin_jsx_pragmatic_1 = __importDefault(require("./babel-plugin-jsx-pragmatic"));
var babel_plugin_1 = __importDefault(require("@emotion/babel-plugin"));
var pragmaName = "___EmotionJSX";
// pull out the emotion options and pass everything else to the jsx transformer
// this means if @babel/plugin-transform-react-jsx adds more options, it'll just work
// and if babel-plugin-emotion adds more options we can add them since this lives in
// the same repo as babel-plugin-emotion
module.exports = function (api, _a) {
    if (_a === void 0) { _a = {}; }
    var pragma = _a.pragma, sourceMap = _a.sourceMap, autoLabel = _a.autoLabel, labelFormat = _a.labelFormat, instances = _a.instances, options = __rest(_a, ["pragma", "sourceMap", "autoLabel", "labelFormat", "instances"]);
    return {
        plugins: [
            [
                babel_plugin_jsx_pragmatic_1.default,
                {
                    export: "jsx",
                    module: "@emotion/react",
                    import: pragmaName,
                },
            ],
            [plugin_transform_react_jsx_1.default, __assign({ pragma: pragmaName, pragmaFrag: "React.Fragment" }, options)],
            [
                babel_plugin_1.default,
                {
                    sourceMap: sourceMap,
                    autoLabel: autoLabel,
                    labelFormat: labelFormat,
                    instances: instances,
                    cssPropOptimization: true,
                },
            ],
        ],
    };
};
