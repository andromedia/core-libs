"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.isDev = void 0;
var config_1 = __importDefault(require("next/config"));
/**
 * XXX Next.js FORCES the NODE_ENV to be either "development" or "production" at build time.
 * Because of this, we have a difference between the process.env.NODE_ENV given by Express and the on given by Next
 * In order to avoid this huge issue, we stored the real NODE_ENV in next.runtimeConfig.js:NODE_ENV
 * And this function must be used to get the NODE_ENV instead of process.env.NODE_ENV
 *
 * This function is compatible with Express/Next, and can be used anywhere, on the client and server.
 *
 * @returns {string}
 * @see XXX https://github.com/zeit/next.js/issues/3605#issuecomment-370255754
 */
exports.isDev = function () {
    var publicRuntimeConfig = config_1.default().publicRuntimeConfig;
    return publicRuntimeConfig.mode === "dev";
};
