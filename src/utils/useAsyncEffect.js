"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.useAsyncEffect = void 0;
var react_1 = require("react");
function useAsyncEffect(action, dependencies, cleanup) {
    react_1.useEffect(function () {
        action();
        if (cleanup) {
            return cleanup;
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, dependencies);
}
exports.useAsyncEffect = useAsyncEffect;
