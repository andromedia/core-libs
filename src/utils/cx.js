"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.cx = void 0;
exports.cx = function () {
    var classNames = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        classNames[_i] = arguments[_i];
    }
    var cx = classNames.join(" ").replace(/\s+/g, " ").split(" ");
    var final = cx;
    // post process css:
    // take last tailwind class from classname.
    final = takeLast(["p-", "w-", "m-"], final);
    console.log(final);
    return final.join(" ");
};
var takeLast = function (keywords, cx) {
    var final = [];
    var last = {};
    var getKeyword = function (k) {
        for (var _i = 0, keywords_1 = keywords; _i < keywords_1.length; _i++) {
            var i = keywords_1[_i];
            if (k.indexOf(i) === 0) {
                return { key: i, value: k };
            }
        }
    };
    cx.forEach(function (e) {
        var kw = getKeyword(e);
        if (kw) {
            last[kw.key] = kw.value;
        }
        else {
            final.push(e);
        }
    });
    if (Object.keys(last).length > 0) {
        for (var i in last) {
            final.push(last[i]);
        }
    }
    return final;
};
