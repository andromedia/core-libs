"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.saxios = void 0;
var axios_1 = __importDefault(require("axios"));
var axios_cookiejar_support_1 = __importDefault(require("axios-cookiejar-support"));
var tough_cookie_1 = __importDefault(require("tough-cookie"));
exports.saxios = axios_1.default.create({});
axios_cookiejar_support_1.default(exports.saxios);
exports.saxios.defaults.jar = new tough_cookie_1.default.CookieJar();
