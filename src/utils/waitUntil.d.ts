export declare function waitUntil(condition: (() => any) | number): Promise<unknown>;
