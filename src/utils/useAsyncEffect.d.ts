declare type AsyncFunction<TResult> = () => Promise<TResult>;
export declare function useAsyncEffect<TResult>(action: AsyncFunction<TResult>, dependencies: any[], cleanup?: () => void): void;
export {};
