export interface Db {
}
export interface IUser {
    username: string;
    password?: string;
    roles: IRole[];
}
export interface IRole {
    name: string;
    features: string[];
}
export interface IDbOptions {
    getUser: (username: string) => Promise<IUser>;
    getAllRoles: () => Promise<IRole[]>;
}
export declare const initializeDb: (options: IDbOptions) => Db;
