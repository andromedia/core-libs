import { IReqBody } from ".";
declare type IFeaturePermission = "*" | string[];
declare type IFeatureBody = Record<string, Record<string, IFeaturePermission>>;
interface IFeatureRoot {
    body: IFeatureBody;
    file: string;
}
export declare const allowFeature: (body: IReqBody) => Promise<IReqBody>;
export declare const getFeatureRoot: (body: IReqBody) => Promise<IFeatureRoot>;
export declare const getFeature: (body: IReqBody) => Promise<IFeaturePermission | null>;
export {};
