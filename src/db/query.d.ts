import { NextApiResponse } from "next";
import { IReqBody } from ".";
export declare const doQuery: (operation: string, body: IReqBody, res: NextApiResponse) => Promise<void>;
