export interface IUser {
    username: string;
    password?: string;
    roles: IRole[];
}
export interface IRole {
    name: string;
    features: string[];
}
export interface IAuthOptions {
    login: (username: string, password: string) => Promise<{
        success: boolean;
        msg: string;
        user?: IUser;
    }>;
    getAllRoles: () => Promise<IRole[]>;
}
export interface Auth extends IAuthOptions {
}
export declare const initializeAuth: (options: IAuthOptions) => Auth;
