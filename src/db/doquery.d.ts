import { NextApiResponse } from "next";
import { IReqBody } from ".";
export declare const doQuery: (body: IReqBody, res: NextApiResponse) => Promise<void>;
