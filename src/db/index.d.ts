import { NextApiRequest, NextApiResponse } from "next";
import { Auth } from "./auth";
export interface IReqBody {
    p: string;
    f: string;
    m: string;
    t: string;
    q: any;
}
export declare const CoreDb: (req: NextApiRequest, res: NextApiResponse, options: ICoreOptions) => Promise<unknown>;
export interface ICoreOptions {
    auth: Auth;
    config: {
        serverRuntimeConfig: any;
        publicRuntimeConfig: {
            mode: string;
        };
    };
}
